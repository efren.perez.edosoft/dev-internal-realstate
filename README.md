# Ejecutar una instancia de flask

1. Crear un proyecto GCP
2. Abrir un terminal en el proyecto GCP
3. Ejecutar:
  a. git clone https://gitlab.com/efren.perez.edosoft/dev-internal-realstate.git
  b. git checkout run_flask
  c. flask --app app run
4. Abrir otro terminal en el proyecto GCP
  a. wget localhost:5000


# Ejecutar Flask como servicio GAE

1. En el proyecto GCP ir a App Engine
2. Clicar sobre Create Application
3. Elegir la región europe-west
4. Elegir lenguaje Python y entorno Standard.
5. Abrir un terminal en el proyecto.
6. Ejecutar:
  a. gcloud init -> Configuramos nuestra cuenta y el proyecto donde vamos a usar App Engine
  b. git checkout first_gae
  c. gcloud app deploy --version=firstgae app.yaml
7. Nos puede dar un error -> ERROR: (gcloud.app.deploy) Error Response: [9]
8. Clicamos sobre el link del log de error
9. Nos abrirá Cloud Build, y clicamos sobre el Build que ha fallado
10. Bajamos hasta el error y buscamos el error "not enabled" 
11. Activamos la API en API & Services -> Enabled APIs & services
12. Ejecutar:
   a. gcloud app deploy --version=firstgae app.yaml
   b. gcloud app browse
13. Clicamos sobre el link

Configurando Cloud SQL

1. Ir a APIs & Services
2. Habilitar las APIs: Compute Engine API, Cloud SQL y Cloud SQL Admin API
3. Ir a SQL -> Create Instance -> Chose PostgreSQL
4. Chose PostgreSQL
5. Elegir un nombre de instancia
  a. Guardar la contraseña que se genera
  b. Seleccionamos Development
  c. Marcamos región europe-west1 (Belgium) y single zone
  d. Abrimos Show configuration options
  e. Machine Type -> Lightwight 1vCPU, 3,75 GB
  f. Storage HDD 10Gb 
  g. Disable automatic storage increases
  h. Public IP
  i. Authorized networks 0.0.0.0/0
  j. Data protection -> Disable automatic backups and enable point-in-time recovery
6. Create Instance
7. Databases -> CREATE DATABASE -> realstate
8. Abrimos un terminal en el proyecto GCP:
  a. git checkout sqlcloud
  b. python3 -m venv $HOME/venv
  c. pip3 install -r requirements.txt
  d. python3 setup.py install
  e. export SQL_URI=postgresql://postgres:[CAMBIAR_PASS]@[CAMBIAR_IP]:5432/realstate
  e. populate_backoffice.py


Desplegar backoffice con acceso a los datos

1. Abrimos un terminal en el proyecto GCP
  a. Modificamos el fichero app.yaml para cambiar la variable de conexión
2. Desplegamos con gcloud app deploy --version=backend app.yaml
3. Ejecutamos gcloud app browse
4. Clicamos sobre el link
5. Añadimos al final ?org_id=0 ó 1 ó 2

Desplegar frontoffice con acceso a los datos usando el usuario postgres

1. Abrimos un terminal en el proyecto GCP
  a. Modificamos el fichero front_app.yaml para cambiar la variable de conexión y el id de organización
2. Desplegamos con gcloud app deploy --version=frontend front_app.yaml
3. Ejecutamos gcloud app browse -s frontend

Popular datos de frontend

1. Abrimos un terminal en el proyecto GCP
  a. Nos aseguramos de tener activo el virtual environment con source $HOME/venv/bin/activate
  b. python3 setup.py install
  c. export SQL_URI=postgresql://postgres:[CAMBIAR_PASS]@[CAMBIAR_IP]:5432/realstate
  d. export ORG_ID=1
  e. export CLIENT_SCHEMA=org_1