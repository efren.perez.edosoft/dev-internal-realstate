from setuptools import setup

setup(
    name='realstate',
    version='',
    packages=['model', 'model.backoffice_model', 'model.frontoffice_model'],
    url='',
    license='',
    author='Efren Perez',
    author_email='efren.perez@edosoft.es',
    description='',
    scripts=['model/backoffice_model/populate_backoffice.py',
             'model/frontoffice_model/populate_frontoffice.py'],
)
