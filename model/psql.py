from flask_sqlalchemy import SQLAlchemy


class PsqlConfig:

    db = SQLAlchemy()

    @classmethod
    def init_db(cls, app):
        cls.db.app = app
        cls.db.init_app(app)
