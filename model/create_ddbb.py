from sqlalchemy import create_engine, inspect
from sqlalchemy import schema as sqlschema

from model.environment import SQL_URI


class DatabaseGenerator:

    @staticmethod
    def create_database(schema, tables):

        sql_engine = create_engine(SQL_URI)
        print(f"Checking schema {schema}")
        if not sql_engine.dialect.has_schema(sql_engine, schema):
            sql_engine.execute(sqlschema.CreateSchema(schema))
            print(f"Schema {schema} - ADDED")
        else:
            print(f"Schema {schema} - OK")

        _inspect = inspect(sql_engine)

        print(f"Checking tables in schema {schema}")
        for table in tables:
            if not _inspect.has_table(table.__tablename__, schema=schema):
                table.__table__.create(bind=sql_engine)
                print(f"Table {table.__tablename__} in schema {schema} - ADDED")
            else:
                print(f"Table {table.__tablename__} in schema {schema} - OK")

    @staticmethod
    def drop_schema(schema, tables):
        sql_engine = create_engine(SQL_URI)
        if not sql_engine.dialect.has_schema(sql_engine, schema):
            return
        _tables = tables.copy()
        _tables.reverse()
        _inspect = inspect(sql_engine)
        for table in _tables:
            if _inspect.has_table(table.__tablename__, schema=schema):
                table.__table__.drop(bind=sql_engine)
        sql_engine.execute(sqlschema.DropSchema(schema))
