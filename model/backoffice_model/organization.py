from model.environment import BACKOFFICE_SCHEMA
from model.psql import PsqlConfig

db = PsqlConfig.db


class Organization(db.Model):
    """
    Organization that will send houses
    """
    __table_args__ = {"schema": BACKOFFICE_SCHEMA}

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)

    def __repr__(self):
        return '<Organization %r>' % self.name
