from model.environment import BACKOFFICE_SCHEMA
from model.psql import PsqlConfig

db = PsqlConfig.db


class HouseType(db.Model):
    """
    Houses types are the different types of houses
    """
    __table_args__ = {"schema": BACKOFFICE_SCHEMA}

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), nullable=False)

    def __repr__(self):
        return '<HouseType %r>' % self.name
