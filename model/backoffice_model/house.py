from model.environment import BACKOFFICE_SCHEMA
from model.backoffice_model.house_type import HouseType
from model.psql import PsqlConfig

db = PsqlConfig.db


class House(db.Model):
    """
    House table
    """

    __table_args__ = {"schema": BACKOFFICE_SCHEMA}

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    house_type_id = db.Column(db.Integer,
                            db.ForeignKey(f'{BACKOFFICE_SCHEMA}.{HouseType.__tablename__}.id'),
                            nullable=False)

    def __repr__(self):
        return '<House %r>' % self.name
