import random

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from model.environment import BACKOFFICE_SCHEMA, SQL_URI
from model.backoffice_model.house import House
from model.backoffice_model.house_subscription import HouseSubscription
from model.backoffice_model.house_type import HouseType
from model.backoffice_model.organization import Organization
from model.create_ddbb import DatabaseGenerator

tables = [HouseType, Organization, House, HouseSubscription]
DatabaseGenerator.drop_schema(BACKOFFICE_SCHEMA, tables)
DatabaseGenerator.create_database(BACKOFFICE_SCHEMA, tables)

sql_engine = create_engine(SQL_URI)
Session = sessionmaker(bind=sql_engine)
session = Session()

org_1 = Organization(name="Empresa 1")
session.add(org_1)
org_2 = Organization(name="Empresa 2")
session.add(org_2)
org_3 = Organization(name="Empresa 3")
session.add(org_3)

normal_house = HouseType(name="Casa Normal")
session.add(normal_house)
premium_house = HouseType(name="Casa Premium")
session.add(premium_house)
vip_house = HouseType(name="Casa VIP")
session.add(vip_house)

session.commit()

house_types = [normal_house.id, premium_house.id, vip_house.id]
house_street = ["Dediró", "Tunera", "Coche", "Libertad", "Clavo", "Viera", "Escudo"]
house_adj = ["Grande", "Pequeña", "Espaciosa", "Alta", "Bajo tierra", "Ventosa", "Ajardinada"]
for index in range(0, 50):
    house_name = "Casa " + house_adj[random.randint(0, 6)] + " en calle " + house_street[random.randint(0, 6)] \
                 + " Número " + str(index)
    session.add(House(name=house_name, reg_type_id=house_types[random.randint(0, 2)]))

session.commit()

session.add(HouseSubscription(organization_id=org_1.id, house_type_id=normal_house.id))
session.add(HouseSubscription(organization_id=org_2.id, house_type_id=normal_house.id))
session.add(HouseSubscription(organization_id=org_2.id, house_type_id=premium_house.id))
session.add(HouseSubscription(organization_id=org_3.id, house_type_id=premium_house.id))
session.add(HouseSubscription(organization_id=org_3.id, house_type_id=vip_house.id))
session.commit()