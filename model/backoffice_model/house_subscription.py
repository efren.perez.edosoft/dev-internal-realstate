from model.backoffice_model.organization import Organization
from model.backoffice_model.house_type import HouseType
from model.psql import PsqlConfig
from model.environment import BACKOFFICE_SCHEMA

db = PsqlConfig.db


class HouseSubscription(db.Model):
    """
    Table to link houses to organizations.
    """
    __table_args__ = {"schema": BACKOFFICE_SCHEMA}

    id = db.Column(db.Integer, primary_key=True)
    organization_id = db.Column(db.Integer,
                                db.ForeignKey(f'{BACKOFFICE_SCHEMA}.{Organization.__tablename__}.id'),
                                nullable=False)
    house_type_id = db.Column(db.Integer,
                            db.ForeignKey(f'{BACKOFFICE_SCHEMA}.{HouseType.__tablename__}.id'),
                            nullable=False)

    def __repr__(self):
        return ''
