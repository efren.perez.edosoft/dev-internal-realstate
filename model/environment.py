import os

BACKOFFICE_SCHEMA = os.getenv("BACKOFFICE_SCHEMA", "backoffice")
SQL_URI = os.getenv("SQL_URI", None)  # Connection URI to perform DDBB operations
ORG_ID = int(os.getenv("ORG_ID", -1))  # Identificador de la organización
CLIENT_SCHEMA = os.getenv("CLIENT_SCHEMA", "org_1")
