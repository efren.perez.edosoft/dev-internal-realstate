from model.backoffice_model.house import House
from model.environment import BACKOFFICE_SCHEMA, CLIENT_SCHEMA
from model.psql import PsqlConfig

db = PsqlConfig.db


class Booking(db.Model):
    """

    """
    __table_args__ = {"schema": CLIENT_SCHEMA}

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True, nullable=False)
    house_id = db.Column(db.Integer,
                         db.ForeignKey(f'{BACKOFFICE_SCHEMA}.{House.__tablename__}.id'),
                         nullable=False)

    def __repr__(self):
        return '<Booking %r>' % self.name
