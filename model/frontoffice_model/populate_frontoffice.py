from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from model.create_ddbb import DatabaseGenerator
from model.environment import CLIENT_SCHEMA, SQL_URI, ORG_ID
from model.frontoffice_model.booking import Booking

tables = [Booking]
DatabaseGenerator.drop_schema(CLIENT_SCHEMA, tables)
DatabaseGenerator.create_database(CLIENT_SCHEMA, tables)

sql_engine = create_engine(SQL_URI)
Session = sessionmaker(bind=sql_engine)
session = Session()

houses = session.execute("SELECT h.id FROM backoffice.house h "
                         "INNER JOIN backoffice.house_type ht on h.house_type_id = ht.id "
                         "INNER JOIN backoffice.house_subscription hs "
                         "on hs.house_type_id = ht.id "
                         "WHERE hs.organization_id = {0}".format(ORG_ID))

for house in houses:
    session.add(Booking(name="Booking {0}".format(int(house[0])), house_id=int(house[0])))
session.commit()
