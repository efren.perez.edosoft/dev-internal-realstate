import sys
import random

from flask import Flask, request, jsonify

from model.backoffice_model.house import House
from model.psql import PsqlConfig
from model.environment import SQL_URI

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = SQL_URI
PsqlConfig.init_db(app)


@app.route('/')
def backoffice():  # put application's code here
    org_id = request.args.get('org_id', default=None, type=int)
    if org_id is None:
        houses = PsqlConfig.db.session.query(House).all()
        houses_json = [{"name": house.name} for house in houses]
    else:
        houses = PsqlConfig.db.session.execute("SELECT h.name FROM backoffice.house h "
                                               "INNER JOIN backoffice.house_type ht on h.house_type_id = ht.id "
                                               "INNER JOIN backoffice.house_subscription hs "
                                               "on hs.house_type_id = ht.id "
                                               "WHERE hs.organization_id = {0}".format(org_id))
        houses_json = [{"name": house[0]} for house in houses]

    return jsonify({"houses": houses_json})
