from flask import Flask, jsonify

from model.frontoffice_model.booking import Booking
from model.psql import PsqlConfig
from model.environment import SQL_URI, ORG_ID

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = SQL_URI
PsqlConfig.init_db(app)


@app.route('/')
def frontoffice():  # put application's code here
    houses = PsqlConfig.db.session.execute("SELECT h.name FROM backoffice.house h "
                                           "INNER JOIN backoffice.house_type ht on h.house_type_id = ht.id "
                                           "INNER JOIN backoffice.house_subscription hs "
                                           "on hs.house_type_id = ht.id "
                                           "WHERE hs.organization_id = {0}".format(ORG_ID))
    houses_json = [{"name": house[0]} for house in houses]
    return jsonify({"houses": houses_json})


@app.route('/booking')
def get_bookings():
    bookings = PsqlConfig.db.session.query(Booking).all()
    bookings_json = [{"name": booking.name} for booking in bookings]
    return jsonify({"bookings": bookings_json})
